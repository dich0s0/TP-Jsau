var axios = require('axios');

export default {
    data() {
      return {
        userId: '',
        id: '',
        title: '',
        body: ''
      }
    },
    methods: {
      // Get request, when the button "Charger data" is clicked.
      created(evt) {
        axios.get(`http://local.test/api`)
        .then((response) => {
          // JSON responses are automatically parsed.
          this.userId = response.data.userId;
          this.id = response.data.id;
          this.title = response.data.title;
          this.body = response.data.body;
          console.log("La donnée a été bien chargée");
        })
        .catch(e => {
          this.errors.push(e)
        })

      },
      // Delete request, when button "Supprimer data" is clicked.
      deleted(evt){
          axios({
                method: 'DELETE',
                url: 'http://local.test/api/user',
                headers: { 'Content-Type': 'application/json' },
                params: {userID: '20'}
              }).then((response) => {
                // JSON responses are automatically parsed.
                console.log(response.data.message);
                this.userId = "";
                this.id = "";
                this.title = "";
                this.body = "";
                console.log("a été bien supprimée");
              });
      },
      // Post request, when button "Ajouter data" is clicked.
      ajouter(evt){
          axios({
                method: 'post',
                url: 'http://local.test/api/user',
                headers: { 'Content-Type': 'application/json' },
                data: {userID: 20, userNom:'Mickael'},

              }).then((response) => {
                // JSON responses are automatically parsed.
                console.log('user avec id = ' + response.data.userId + ' a été ajouté');
                this.userId = "20";
                this.id = "2";
                this.title = "iPhone X. La révolution n'a pas de prix";
                this.body = "A l'occasion de l'anniversaire de l'iPhone, Apple s'invite dans votre quotidien pour partager sa vision du futur.";
              });
      },
      // Put request, when button "Modifier data" is clicked.
      modifier(evt){
          axios({
                method: 'put',
                url: 'http://local.test/api/user',
                headers: { 'Content-Type': 'application/json' },
                data: {userID: 20, userNom:'Mickael'}
              }).then((response) => {
                // JSON responses are automatically parsed.
                console.log(response.data.message);
                this.userId = "20";
                this.id = "2";
                this.title = "iPhone X. La révolution n'a pas de prix";
                this.body = "A l'occasion de l'anniversaire de l'iPhone, Apple s'invite dans votre quotidien pour partager sa vision du futur.";
              });
      }
    }
}
