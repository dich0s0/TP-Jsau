
// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8081;        // set our port


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Une requete reçue:');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8081/api)
router.get('/', function(req, res) {
    console.log('GET sans parametre')
    res.send({
                "userId": 1,
                "id": 1,
                "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
              });
});

router.post('/user',function(req, res) {
    console.log('POST avec userId = ' + req.body.userID)
    res.send({userId: req.body.userID});
});

router.put('/user',function(req, res) {
    console.log('PUT avec userId = ' + req.body.userID)
    res.send({message: 'La donnée a été bien modifiée'});
});

router.delete('/user', function(req, res) {
    console.log('DELETE avec userId = ' + req.query.userID)
    res.send({message: 'user id = ' + req.query.userID});
});

// more routes for our API will happen here
router.post('/desktop', function(req, res){
  console.log(req.body);
  res.sendStatus(200);
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Things are happening on port : ' + port);
