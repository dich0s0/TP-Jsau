'use strict'

const my_shared_code_headless = require ('./my_shared_code_headless')

function writeContent(body_elem) {
  let numbers = my_shared_code_headless.generateEvenNumbers(20)
  npair(body_elem, numbers,0)
}
function npair(body_elem, numbers, nb){
  if (nb==numbers.length){return}
  setTimeout(function(){
    body_elem.innerHTML=body_elem.innerHTML +' '+ numbers[nb]
    npair(body_elem, numbers, nb+1)
  }, 1000)
}

module.exports = {
  writeContent
}
