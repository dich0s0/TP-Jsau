'use strict'

function generateEvenNumbers(nombre) {
  var numbers = [];
  var pair = 0;
  for (var i=0; i<nombre; i++){
    numbers.push(pair);
    pair+=2;
  }
    return numbers;
}

module.exports = {
    generateEvenNumbers
}
