'use strict'

const my_shared_code_headless = require ('./my_shared_code_headless.js')
const my_shared_code_ui = require('./my_shared_code_ui.js')
const express = require('express')
const app = express()

app.use('/', express.static(__dirname + '/public'))


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})


let even_numbers = my_shared_code_headless.generateEvenNumbers()


app.get('/info', function(req, res){
res.send('jsau-webserver-1.0.0')
})
