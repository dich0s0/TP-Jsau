'use strict'

var fs = require('fs');

function writeContent (path, callback){
  var tab = [];
fs.readdir(path, function(err, items) {
    for (var i=0; i<items.length; i++) {
        tab.push(items[i]);
    }
    callback(tab);
  });
}
  module.exports = {
      writeContent
  }
